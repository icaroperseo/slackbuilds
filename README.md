# Personal SlackBuilds for Slackware

The following _SlackBuild_ files were created for the current **stable branch** of Slackware (14.2), but could quite possibly works for previous verisiones with few (or none) modifications.

If you find any issues or would like to propose some improvement, just let me know, **contributions are welcome!**

The following _SlackBuild_ files are currently avaliable in this repository:

| Name            | Description                                                       | Hosted in SBo |
| ----            | -----------                                                       | ------        |
| translate-shell | Command-line translator                                           |               |
| git-crypt       | Transparent file encryption in Git                                |               |
| imgmin          | Automated lossy JPEG optimization                                 | yes           |
| qgit            | GIT GUI viewer built on Qt/C++                                    |               |
| dianara         | Pump.io client written using Qt4                                  | yes           |
| qdc             | Castilian dictionary written using Qt5                            |               |
| goldendict      | Dictionary lookup program                                         |               |
| ptask           | A GTK+ graphical user interface for managing tasks in taskwarrior |               |

**Note:** If the _SlackBuild_ file that you want appears in the previous list but not on this repository, then it is most likely because it was included in [SlackBuilds.org](https://slackbuilds.org/) (SBo). See the _SBo_ branch on this repository to confirm this.
